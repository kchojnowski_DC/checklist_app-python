# -- coding: utf-8 --

import sys
from PyQt5.QtWidgets import QApplication
from MainWindow import MainWindow
from LoginDialog import LoginDialog

if __name__ == '__main__':
    app = QApplication(sys.argv)

    login = LoginDialog()

    if not login.exec_():
            sys.exit(-1)

    window = MainWindow()
    window.show()
    sys.exit(app.exec_())