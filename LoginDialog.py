# -- coding: utf-8 --

from PyQt5.QtWidgets import QLineEdit,QDialogButtonBox,QFormLayout,QDialog,QMessageBox
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
import qdarkstyle


class LoginDialog(QDialog):
    def __init__(self, parent=None):
        super(LoginDialog,self).__init__(parent)
        self.init_ui()


    def init_ui(self):

        ### delete question mark
        self.setWindowFlags(self.windowFlags()
                            ^ Qt.WindowContextHelpButtonHint)

        ### login & password fields
        self.username = QLineEdit(self)
        self.password = QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        loginLayout = QFormLayout()
        loginLayout.addRow("Username", self.username)
        loginLayout.addRow("Password", self.password)
        self.buttons = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        self.buttons.accepted.connect(self.users_control)
        self.buttons.rejected.connect(self.reject)
        layout = QtWidgets.QVBoxLayout(self)
        layout.addLayout(loginLayout)
        layout.addWidget(self.buttons)
        self.setLayout(layout)

        ### set window title & stylesheet
        self.setWindowTitle('WAREX Checklist')
        self.setStyleSheet((qdarkstyle.load_stylesheet_pyqt5()))
        ###lock resize
        self.setSizeGripEnabled(False)
        self.setFixedSize(self.sizeHint())

    def users_control(self):
        if (self.username.text() == 'test' and self.password.text() == 'test'):
            self.accept()
        else:
            QMessageBox.warning(self, 'Error', 'Wrong username or password')






